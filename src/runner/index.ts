/** Dependencies */
import { NodeBlock, validator } from "tripetto-runner-foundation";

export class Stop extends NodeBlock<{
    readonly imageURL?: string;
    readonly imageWidth?: string;
    readonly imageAboveText?: boolean;
}> {
    @validator
    stopHere(): boolean {
        return false;
    }
}
