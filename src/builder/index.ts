/** Package information retrieved from `package.json` using webpack. */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

import {
    Forms,
    NodeBlock,
    REGEX_IS_URL,
    definition,
    editor,
    insertVariable,
    isString,
    pgettext,
    tripetto,
} from "tripetto";

/** Assets */
import ICON from "../../assets/icon.svg";

@tripetto({
    type: "node",
    identifier: PACKAGE_NAME,
    version: PACKAGE_VERSION,
    kind: "headless",
    icon: ICON,
    get label() {
        return pgettext("block:stop", "Force stop");
    },
})
export class Stop extends NodeBlock {
    @definition("string", "optional")
    imageURL?: string;

    @definition("boolean", "optional")
    imageAboveText?: boolean;

    @definition("string", "optional")
    imageWidth?: string;

    @editor
    defineEditor(): void {
        this.editor.form({
            title: pgettext("block:stop", "Explanation"),
            controls: [
                new Forms.Static(
                    pgettext(
                        "block:stop",
                        "Stops the form without completing it. This assures no data for the form will be submitted."
                    )
                ).markdown(),
            ],
        });

        this.editor.name(true, true);
        this.editor.description();

        this.editor.option({
            name: pgettext("block:stop", "Image"),
            form: {
                title: pgettext("block:stop", "Image"),
                controls: [
                    new Forms.Text(
                        "singleline",
                        Forms.Text.bind(this, "imageURL", undefined)
                    )
                        .label(pgettext("block:stop", "Image source URL"))
                        .inputMode("url")
                        .placeholder("https://")
                        .action("@", insertVariable(this))
                        .autoValidate((ref: Forms.Text) =>
                            ref.value === ""
                                ? "unknown"
                                : REGEX_IS_URL.test(ref.value) ||
                                  (ref.value.length > 23 &&
                                      ref.value.indexOf(
                                          "data:image/jpeg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/png;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/svg;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 22 &&
                                      ref.value.indexOf(
                                          "data:image/gif;base64,"
                                      ) === 0) ||
                                  (ref.value.length > 1 &&
                                      ref.value.charAt(0) === "/")
                                ? "pass"
                                : "fail"
                        ),
                    new Forms.Text(
                        "singleline",
                        Forms.Checkbox.bind(this, "imageWidth", undefined)
                    )
                        .label(pgettext("block:stop", "Image width (optional)"))
                        .width(100)
                        .align("center"),
                    new Forms.Checkbox(
                        pgettext(
                            "block:stop",
                            "Display image on top of the paragraph"
                        ),
                        Forms.Checkbox.bind(this, "imageAboveText", undefined)
                    ),
                ],
            },
            activated: isString(this.imageURL),
        });

        this.editor.groups.options();
        this.editor.visibility();
    }
}
